console.info("Socket.io chat test client");

var cleanupOnly = (process.argv.length > 2 ? (process.argv[2] == 'cleanup') : false);

var serverRoot = 'localhost:3000'; // server domain, without http://
var auth = 'khiem:a'; // username:password for Basic Auth

var TOTAL_CONNECTIONS = 500;
var TOTAL_GROUPS = 200;
var GROUP_SIZE = TOTAL_CONNECTIONS/2;
var MSG_PER_GROUP = Math.floor(GROUP_SIZE/5);

var io = require('socket.io-client');
var sleep = require('sleep');
var request = require('request');

var prefix = require('process').pid;
 
var sockets = [];
var socketHash = {};

var testUsers = [],
    testGroups = [],
    userGroups = {};

var createdUsers = 0,
    createdGroups = 0,
    totalConnected = 0,
    msgSent = 0,
    msgReceived = 0;

var expectedMsgSend = TOTAL_GROUPS * MSG_PER_GROUP;
var expectedMsgRecv = expectedMsgSend * GROUP_SIZE; 

var connectionLogs = {};
var totalConnectElapse = 0,
    totalMsgRoudtrip = 0;

var randomInt = function (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

function prepareData() {
    // create users
    for (var idx = 0; idx < TOTAL_CONNECTIONS; idx++) {

        (function(name) {
            request.post({
                url: 'http://' + auth + '@' + serverRoot + '/rest/user',
                headers: { 'content-type': 'application/json' },
                body: '{ "username": "' + name + '", "email": "' + name + '@gmail.com", "password": "a" }'
            }, function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    createdUsers++;
                    // got body.userId
                    var userId = JSON.parse(body).userId;
                    
                    testUsers.push({ id: userId, username: name});
                    
                    userGroups[userId] = [];

                    if (createdUsers == TOTAL_CONNECTIONS) {
                        console.info('Created ' + createdUsers + ' users');
                        sleep.sleep(1);
                         // create group
                        createGroups();
                    }

                } else {
                    console.error('Error when creating users', error, response.statusCode);
                    process.exit(1);
                }
            });
        })('test_user_' + prefix + '_' + idx);
    }    
}


function createGroups() {
    console.log('Users', testUsers);
    for (var idx = 0; idx < TOTAL_GROUPS; idx++) {
        (function(name) {
            // randomly select users to add to group
            var membersIds = [];
            while (membersIds.length < GROUP_SIZE) {
                var id = testUsers[randomInt(0, TOTAL_CONNECTIONS - 1)].id;
                if (membersIds.indexOf(id) < 0) {
                    membersIds.push(id);
                }
            }
            console.log('Creating group ' + name + ', members ' + JSON.stringify(membersIds));

            request.post({
                url: 'http://' + auth + '@' + serverRoot + '/rest/group',
                headers: { 'content-type': 'application/json' },
                body: '{ "title": "' + name + '", "members": ' + JSON.stringify(membersIds) + '}'
            }, function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    createdGroups++;
                    var groupId = JSON.parse(body).groupId;
                    console.log('Group ' + groupId + ', member: ' + JSON.stringify(membersIds));
                    // got body.groupId
                    testGroups.push({ id: groupId, title: name, membersIds: membersIds });

                    for (var idx2 in membersIds) {
                        var memId = membersIds[idx2];
                        if (userGroups[memId].indexOf(groupId) < 0) {
                            userGroups[memId].push(groupId);
                        }
                    }

                    if (createdGroups == TOTAL_GROUPS) {
                        console.info('Created ' + createdGroups + ' groups');

                        // trigger load test now
                        sleep.sleep(2);

                        connect();
                    }

                } else {
                    console.error('Error when creating groups', error, response.statusCode);
                    process.exit(1);
                }
            });
        })('test_group_' + prefix + '_' + idx);
    }    
}

function cleanTestData() {
    console.info('Cleaning up test data ...');
    request.post({
        url: 'http://' + auth + '@' + serverRoot + '/rest/reset_db',
    }, function(err, response, body) {
        console.log(err, response.statusCode, body);
        if (cleanupOnly) process.exit();
        if (!err && response.statusCode == 200) {
            sleep.sleep(2);
            // continue test
            prepareData();
        }
    });
}

function connect() {
    for (var socket_n = 0; socket_n < TOTAL_CONNECTIONS; socket_n++) {
     
        (function(j) {
            // issue REST request to get token
            var testUserName = testUsers[j].username;
            var testUserId = testUsers[j].id;

            request({
                url: 'http://' + testUserName + ':a@' + serverRoot + '/rest/get_token',
            }, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log(body) // Show the HTML for the Google homepage.
                    var token = JSON.parse(body).token;

                    connectionLogs[j] = {};
                    console.log(j + ': connecting ');
                    connectionLogs[j]['start'] = new Date().getTime();
                    
                    var socket = io.connect('http://' + serverRoot, {
                        'force new connection': true,
                        transports: ['websocket'],
                        query: 'token=' + token
                    });

                    socket.on('chat-message', function (msgObj) {
                        msgReceived++;
                        var elapse = (new Date().getTime() - msgObj.timestamp);
                        totalMsgRoudtrip += elapse;
                        console.info(msgReceived + ': Got message ' + JSON.stringify(msgObj) + ', elapse = ' + elapse);
                        if (msgReceived == expectedMsgRecv) {
                            console.log('RECEIVED ALL ' + msgReceived + ' MESSAGES');
                            console.log('AVERAGE MSG ROUNDTRIP = ' + (totalMsgRoudtrip/msgReceived));
                        }
                    });

                    sockets.push(socket);
                    socketHash[testUserId] = socket;

                    (function(userId, username) {
                        var inner_socket = socket;
                        inner_socket.userId = userId;
                        inner_socket.username = username;
                        inner_socket.on('connect', function () {

                            connectionLogs[j]['connected'] = new Date().getTime();

                            totalConnected++;

                            console.info("Connected[" + j + "] => " + username);
             
                            if (totalConnected == TOTAL_CONNECTIONS) {
                                login();
                            }
             
                            /*var interval = Math.floor(Math.random()*10001) + 5000;
                            setInterval(function() {
                                inner_socket.emit('user message', "Regular timer message every " + interval + " ms");
                            }, interval);*/

                            //sleep.sleep(1);

                            //inner_socket.emit('chat-message', { groupId: 1, fromId: name, msg: 'Message from ' + name });
                        });
                    })(testUserId, testUserName);
             
                    socket.on('error', function (err_msg) {
                        console.error("Connection Error:" + err_msg);
                        console.info('Shutting down test ...');
                        process.exit(1);
                    });                    
             
                    socket.on('disconnect', function () {
                        console.info("Disconnected");
                    });
                }
            });

            //sleep.sleep(1);

        })(socket_n);
    };
}

function login() {
    for (idx in connectionLogs) {
        var elapse = (connectionLogs[idx]['connected'] - connectionLogs[idx]['start']);
        console.log(idx +': ' + elapse);
        totalConnectElapse += elapse;
    }

    console.log('AVERAGE TIME FOR ESTABLISHING CONNECTION = ' + (totalConnectElapse/TOTAL_CONNECTIONS));

    // login
    for (var idx in sockets) {
        if (idx % 10 == 0) {
            sleep.sleep(1);
        }
        sockets[idx].emit('login', { id: sockets[idx].userId, username: sockets[idx].username });
    }

    console.info('All login now, relaxing ...');

    // relax
    sleep.sleep(5);

    performLoadTest();
}

function performLoadTest() {

    console.log('Load test is about to start ...');

    console.log('Total connection = ' + sockets.length);
    console.log('Total group = ' + testGroups.length);

    //process.exit();
    // send messages to groups

    // for each group, take GROUP_SIZE/2 users and send messages to that group
    // messages sent = TOTAL_GROUPS * floor((GROUP_SIZE + 1)/2)
    // total messages = TOTAL_GROUPS * floor((GROUP_SIZE + 1)/2) * GROUP_SIZE
    for (var idx in testGroups) {
        var groupId = testGroups[idx].id;
        var groupTitle = testGroups[idx].title;
        var membersIds = testGroups[idx].membersIds;
        for (var idx2 = 0; idx2 < MSG_PER_GROUP; idx2++) {
            (function() {
                var randIdx = randomInt(0, GROUP_SIZE - 1);
                var socket = socketHash[membersIds[randIdx]];
                socket.emit('chat-message', {
                    fromId: socket.userId,
                    msg: 'Message from ' + socket.username + ' to group id ' + groupId,
                    groupId: groupId
                });

                msgSent++;
                console.log('Sent ' + msgSent + ' messages');

                if (msgSent == expectedMsgSend) {
                    console.log('ALL ' + msgSent + ' MESSAGES SENT');
                }
            })();
        }
    }

    /*for (var idx in sockets) {
        console.log(idx);
        (function(inner_socket) {
            var userId = inner_socket.userId;
            var username = inner_socket.username;
            var groups = userGroups[userId];
            var randGroupId = groups[utils.randomInt(0, groups.length - 1)];   

            inner_socket.emit('chat-message', {
                fromId: inner_socket.userId,
                msg: 'Message from ' + username + ' to group id ' + randGroupId,
                groupId: randGroupId
            });

            msgSent++;
            console.log('Sent ' + msgSent + ' messages');


        })(sockets[idx]);
        
    }*/

}


cleanTestData();
setInterval(function() {
    if (msgReceived > 0 && msgReceived < expectedMsgRecv) {
        console.log('Average msg roudtrip = ' + (totalMsgRoudtrip/msgReceived));
    }
}, 10000);
